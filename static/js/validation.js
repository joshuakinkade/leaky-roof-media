var Validator = {

  sayHello: function() {
    console.log("Hello");
  },

  isValidUsername: function(name) {
    var pattern = /^[\w\.\-]+$/;

    return pattern.test(name);
  },

  isValidEmail: function(email) {
    var pattern = /^[\w\.\-]+@\w+\.\w+$/i;

    return pattern.test(email);
  },

  isValidPassword: function(password) {
    var pattern = /^[^\s][\w@&\-\.\*\$]+$/i;

    console.log(pattern.test(password));

    return pattern.test(password) && password.length > 6;
  },

  passwordsMatch: function (p1,p2) {
    return p1 === p2;
  }

}
