function Blog(id,name,url) {
  var self = this;
  this.id = ko.observable(id);
  self.name = ko.observable(name);
  self.url = ko.observable(url);
  self.hashedURL = ko.computed(function(){
    return "#/" + self.url();
  });
}

function Post(id,blog_id,title,url,sample,content) {
  var self = this;
  self.id = ko.observable(id);
  self.blog_id = ko.observable(blog_id);
  self.title = ko.observable(title);
  self.url = ko.observable(url);
  self.sample=ko.observable(sample);
  self.content = ko.observable(content);
  self.hashedURL = ko.computed(function(){
    return "#/" + self.url();
  });
}

function BlogsViewModel() {
  var self = this;
  self.blogs = ko.observableArray([new Blog(0,"All", "")]);
  self.recentPosts = ko.observableArray([]);
  self.recentPostsLength = ko.computed(function(){
    if (self.recentPosts() == null) {
      return 0;
    };
    return self.recentPosts().length;
  });
  self.editingPost = ko.observable();
  self.showControlPanel = ko.observable(false);

  self.isStaff = ko.computed(function(){
    return window.sessionStorage["isStaff"] == "True";
  });

  self.getRecentPosts = function(blog_id) {
    $.getJSON("/static/posts.json",function(data){
      var posts = [];
      $.each(data,function(index,post) {
        posts.push(new Post(post.id,post.blog_id,post.title,post.url,post.sample,post.content));
      });

      if (blog_id) {
        posts = _.filter(posts,function(post){
          return post.blog_id() == blog_id;
        });
      };

      self.recentPosts(posts);
    });
  };

  self.filterByBlog = function(blog) {
    console.log("Hello")
  };
}

var viewModel = new BlogsViewModel();

$(document).ready(function() {
  ko.applyBindings(viewModel);

  viewModel.filterByBlog("stuff");

  console.log(viewModel.isStaff());

});
