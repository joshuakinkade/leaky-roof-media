from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.template import RequestContext

def home(request):

  if request.user.is_authenticated():
    c = {'loggedin':True,
         'isStaff': request.user.is_staff,
         'username':request.user.username}
  else:
    c = {'loggedin':False}

  return render_to_response('home.html',c,context_instance=RequestContext(request))
