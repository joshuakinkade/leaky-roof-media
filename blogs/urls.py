from django.conf.urls import patterns, url

urlpatterns = patterns('blogs.views',
  url(r'^$', 'index'),
  url(r'^blogs$', 'blogs'),
  url(r'^posts$', 'posts'),
  url(r"^control_panel$","control_panel"),
  url(r"^control_panel/blogs$","cp_blogs"),
  url(r"^control_panel/posts$","cp_posts"),
  url(r"^control_panel/postData$","postData"),
  #url(r"^control_panel/(?P<blog_name>\w+)/(?P<post_title>\w+)$","cp_post"),  
  url(r"^control_panel/new_post$","new_post"),
  url(r"^control_panel/(?P<blog_name>\w+)/(?P<post_title>\w+)$", "post_editor"),
  url(r'^(?P<blog_name>\w+)$', 'blog'),
  url(r'^(?P<blog_name>\w+)/(?P<post_title>\w+)$', 'post'),
  )
