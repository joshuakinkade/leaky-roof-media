from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.template import RequestContext
from django.core.context_processors import csrf
from django.views.decorators.csrf import ensure_csrf_cookie
from django.contrib.auth.decorators import permission_required
from django.core import serializers
from blogs.models import Blog, Post
from django.http import Http404, HttpResponse
from django.utils import timezone
from datetime import datetime

def index(request):
  print "*Index*"

  if request.user.is_authenticated():
    c = {'loggedin':True,
         'isStaff':request.user.is_staff,
         'username':request.user.username}
  else:
    c = {'loggedin':False}

  return render_to_response('blogs/index.html',c,context_instance=RequestContext(request))


def blogs(request):

  response = serializers.serialize("json",Blog.objects.all())

  return HttpResponse(response)

def posts(request):

  posts = Post.objects.filter(is_published=True,date_published__lt=timezone.now())

  posts = posts.order_by("-date_published")

  response = serializers.serialize("json",posts,use_natural_keys=True)

  return HttpResponse(response)

@permission_required("blogs.can_change_post",raise_exception=True)
def cp_blogs(request):
  print "*CP Blogs*"

  response = serializers.serialize("json",Blog.objects.filter(owner=request.user))

  return HttpResponse(response)

@permission_required("blogs.can_change_post")
def cp_posts(request):
  print "*CP Posts*"

  response = serializers.serialize("json",Post.objects.filter(author=request.user))

  # print response
  return HttpResponse(response)

def blog(request, blog_name):

  print "*Blog*"

  if request.method == "GET":
    #look for blog in database here
    c = {'name':blog_name}
    return render_to_response('blogs/blog.html',c)

  elif request.method == "POST":
    name = request.POST["name"]
    url = request.POST["url"]

    response = 0;

    if Blog.objects.filter(url=url).count() == 0:

      blog = Blog(owner=request.user,name=name,url=url)

      blog.save()

      response = blog.pk

    return HttpResponse(response)

def post(request, blog_name, post_title):

  print "*Post*"

  if request.user.is_authenticated():
    c = {'loggedin':True,
         'isStaff':request.user.is_staff,
         'username':request.user.username}
  else:
    c = {'loggedin':False}

  if request.method == "GET":

    blog = Blog.objects.get(url=blog_name)
    post = Post.objects.get(blog=blog,url=post_title)

    c.update({'blog_url': blog_name,'post_url':post_title,'blog_name':blog.name,'post_title':post.title})
    return render_to_response('blogs/post.html',c)
  elif request.method == "POST":

    content = request.POST["content"]
    is_published = request.POST["is_published"]
    date_published = timezone.now()

    blog = Blog.objects.get(url=blog_name)
    post = Post.objects.get(blog=blog,url=post_title)

    try:
      request.POST["date_published"]
      date_published = datetime.strptime(request.POST["date_published"],"%Y-%m-%dT%H:%M:%S.%f%Z")
    except Exception, e:
      pass

    post.content = content

    if (is_published == "true") and (post.is_published == False):
      print "Publishing"
      post.is_published = True
      post.date_published = date_published

    post.save()

    return HttpResponse("Success")

def postData(request):
  print "*Post Data*"

  post_url = request.GET["post"]
  blog_url = request.GET["blog"]

  blog = Blog.objects.get(url=blog_url)
  post = Post.objects.get(blog=blog,url=post_url)

  response = serializers.serialize("json",Post.objects.filter(blog=blog,url=post_url),use_natural_keys=True)

  return HttpResponse(response)

@ensure_csrf_cookie
@permission_required("blogs.can_change_blog",raise_exception=True)
def control_panel(request):
  print "*Control Panel*"

  c = {'loggedin':True,
     'isStaff':request.user.is_staff,
     'username':request.user.username}
  c.update(csrf(request))
  return render_to_response('blogs/control_panel.html',c)

def cp_post(request):
  pass

@permission_required("blogs.can_change_blog",raise_exception=True)
def new_post(request):
  print "*New Post*"

  blog = Blog.objects.get(url=request.POST["blog_url"])
  test = Post.objects.filter(blog=blog,title=request.POST["title"])
  url = request.POST["post_url"]

  if test.count() == 0:
    url = request.POST["title"].lower().replace(" ","_")
    post = Post(author=request.user,blog=blog,title=request.POST["title"],url=url,content="")

    post.save()

    c = {'loggedin':True,
         'isStaff':request.user.is_staff,
         'username':request.user.username,
         "url":url,
         "blog_url":blog.url}

    c.update(csrf(request))
    return render_to_response("blogs/post_editor.html",c)

@ensure_csrf_cookie
@permission_required("blogs.can_change_blog",raise_exception=True)
def post_editor(request, blog_name, post_title):
  print "*Post Editor*"

  blog = Blog.objects.get(url=blog_name)
  post = Post.objects.get(blog=blog,url=post_title)

  c = {'loggedin':True,
       'isStaff':request.user.is_staff,
       'username':request.user.username,
       "url":post.url,
       "blog_url":blog.url,
       "blog_name":blog.name,
       "post_title":post.title}

  return render_to_response('blogs/post_editor.html',c)
