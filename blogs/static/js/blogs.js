function Blog(id,name,url) {
  var self = this;
  self.id = ko.observable(id);
  self.name = ko.observable(name);
  self.url = ko.observable(url);

}

function Post(id,author,blog_id,title,url,content) {
  var self = this;
  self.id = ko.observable(id);
  self.author = ko.observable(author);
  self.blog_id = ko.observable(blog_id);
  self.title = ko.observable(title);
  self.url = ko.observable(url);
  self.content = ko.observable(content);
  self.date_published = ko.observable();
  self.sample = ko.computed(function(){
    return self.content().slice(0,Math.min(500,self.content().length));
  });
  self.full_url = ko.computed(function(){
    var blog_url;
    $.each(viewModel.blogs(),function(index,blog){
      if(blog.id() === self.blog_id())
      {
        blog_url = blog.url();
      }
    });

    return blog_url + "/" + self.url();
  });  
  self.date_published = ko.observable();
  self.prettyDate = ko.computed(function(){
    var date = Date.parse(self.date_published());
    date = new Date(date);
    var string = date.getMonth() + "/" + date.getDate() + "/" + date.getFullYear();
    string += " " + date.getHours() + ":" + date.getMinutes();

    return string;
  });
}

function BlogsViewModel() {
  var self = this;
  self.blogs = ko.observableArray();
  self.recentPosts = ko.observableArray([]);
  self.recentPostsLength = ko.computed(function(){
    if (self.recentPosts() == null) {
      return 0;
    };
    return self.recentPosts().length;
  });
  self.selectedBlog = ko.observable(null);

  self.editingPost = ko.observable();
  self.showControlPanel = ko.observable(false);

  self.isStaff = ko.computed(function(){
    return window.sessionStorage["isStaff"] == "True";
  });

  self.visiblePosts = ko.observableArray();

  self.getRecentPosts = function() {
    $.getJSON("posts",function(data){
      $.each(data,function(index,post) {
        self.recentPosts.push(new Post(post.pk,post.fields.author,post.fields.blog,post.fields.title,post.fields.url,post.fields.content));
        self.showAllPosts();
      });
    });
  }

  self.getBlogs = function() {
    $.getJSON("blogs",function(data){
      $.each(data,function(index,blog){
        self.blogs.push(new Blog(blog.pk,blog.fields.name,blog.fields.url));
      });
    });
  }

  self.filterByBlog = function(blog) {
    self.visiblePosts.removeAll();
    $.each(self.recentPosts(),function(index,post){
      console.log(blog.id(), post.blog_id());
      if( post.blog_id() === blog.id())
      {
        self.visiblePosts.push(post);
      }
      // else
      // {
      //   self.visiblePosts.remove(post);
      // }
    });
  }

  self.showAllPosts = function() {
    self.visiblePosts.removeAll();
    $.each(self.recentPosts(), function(index,post){
      self.visiblePosts.push(post);
    });
  }

}

var viewModel = new BlogsViewModel();

$(document).ready(function() {
  ko.applyBindings(viewModel);

  viewModel.getBlogs();

  viewModel.getRecentPosts();

});
