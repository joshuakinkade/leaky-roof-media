
function PostViewModel() {
  var self = this;
  self.author = ko.observable();
  self.title = ko.observable();
  self.content = ko.observable();
  self.isPublished = ko.observable();
  self.publishDate = ko.observable();
  self.publishNow = ko.observable(true);
  self.publishLater = ko.observable(false);
  self.rawPubDate = ko.observable("");

  self.load = function() {
    $.getJSON("/blogs/control_panel/postData",{blog: blog_url, post: post_url},
      function(data,status,request){
        self.author(data[0].fields.author);
        self.title (data[0].fields.title);
        self.content(data[0].fields.content);
        self.publishDate(new Date(data[0].fields.date_publshed));
        self.isPublished(data[0].fields.is_published);
      });
  }

  self.save = function() {
    data = {
      content: self.content(),
      is_published: self.isPublished(),
    }

    url = "/blogs/" + blog_url +"/" + post_url;

    $("#status").html("Saving...");

    //console.log(self.publishNow());

    

    //console.log($("#pub_now_opt").checked);

    // $.ajax(
    //   {url:url,
    //    method: "POST",
    //    headers: {"X-CSRFToken": csrf_token},
    //    data: data,
    //    success: function(data,status,request){
    //     $("#status").html("Saved");
    // }});
  }

  self.publish = function() {

    self.isPublished(true);
    self.publishDate(new Date);

    data = {
      content: self.content(),
      is_published: self.isPublished(),
      date_published: self.publishDate().toISOString()
    }
    
    $("#status").html("Publishing...");

    url = "/blogs/" + blog_url +"/" + post_url;

    $.ajax(
      {url:url,
       method: "POST",
       headers: {"X-CSRFToken": csrf_token},
       data: data,
       success: function(data,status,request){
        $("#status").html("Published");
       }
    });
  }
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var viewModel = new PostViewModel();
var csrf_token;

$(document).ready(function(){

  // $("input[type=date]").appendDtpicker();

  csrf_token = getCookie("csrftoken");

  viewModel.load();

  ko.applyBindings(viewModel);

});
