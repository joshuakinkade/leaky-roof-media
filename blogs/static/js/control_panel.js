var csrftoken;

function Blog(id,name,url,dateCreated) {
  var self = this;

  self.newPostName = ko.observable("Stuff");

  self.newPostFormVisible = ko.observable(false);
  self.showNewPostForm = function() {
    self.newPostFormVisible(true);
  }
  self.hideNewPostForm = function() {
    self.newPostFormVisible(false);
    self.newPostName("");
  }

  self.post_url = ko.computed(function(){
    return self.newPostName().toLowerCase().replace("_"," ");
  });

  self.id = ko.observable(id);
  self.name = ko.observable(name);
  self.url = ko.observable(url);
  self.dateCreated = ko.observable(dateCreated);
  self.posts = ko.observableArray();
  self.prettyDate = ko.computed(function(){
    var daysOfWeek = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
    var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
  });
}

function Post(id, blog, title,url,dateCreated,content) {
  var self = this;
  self.id = ko.observable(id);
  self.blog = ko.observable(blog);
  self.title = ko.observable(title);
  self.url = ko.observable(url);
  self.dateCreated = ko.observable(dateCreated);
  self.content = ko.observable(content);
  self.editor_url = ko.computed(function(){
    var blog_url;
    $.each(viewModel.blogs(),function(index,blog){
      if(blog.id() === self.blog())
      {
        blog_url = blog.url();
      }
    });

    return "control_panel/" + blog_url + "/" + self.url();
  });
}

function CP_ViewModel() {
  var self = this;

  self.blogs = ko.observableArray();
  self.posts = ko.observableArray();

  self.getBlogs = function() {
    $.getJSON("control_panel/blogs",function(data){
      $.each(data,function(index,b){
        self.blogs.push(new Blog(b.pk,b.fields.name,b.fields.url,new Date(b.fields.date_created)));
      });
        self.getPosts();
    })
  }

  self.getPosts = function() {
    $.getJSON("control_panel/posts",function(data){
      $.each(data,function(index,p){
        post = new Post(p.pk, p.fields.blog, p.fields.title, p.fields.url, new Date(p.fields.date_created), p.fields.content);

        $.each(self.blogs(),function(index,b){
          if(b.id() === post.blog())
          {
            b.posts.push(post);
          }
        });
      });
    })
  }

  self.createBlog = function() {
    var name = prompt("Enter a name for your new blog: ");

    if( name.length == 0 )
    {
      return;
    }

    var url = name.toLowerCase().split(" ").join("_").replace(".","");
    $.ajax({
      url: url,
      method: "POST",
      headers: {"X-CSRFToken": csrftoken},
      data: {name: name, url: url},
      success: function(data,status,request) {
        if( data != 0 )
        {
          self.blogs.push(new Blog( data, name, url, new Date()));          
        }
        else
        {
          alert("\"" + name + "\" is already used.");
        }
      }
    })
  }
}

// using jQuery - taken from django documentation
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

var viewModel = new CP_ViewModel();

$(document).ready(function(){
  csrftoken = getCookie('csrftoken');
  ko.applyBindings(viewModel);

  viewModel.getBlogs();
});
