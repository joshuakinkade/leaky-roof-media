Index
-----
- url: /
- view: index
- paramaters: none
- return: index.html

Data Routes
-----------
- url: /blogs
- view: blogs
- parameters: none
- returns: all of the blogs in the database

- url: /posts 
- view: posts
- parameters:
  - startDate: get posts after the given date
  - endDate: get posts before the given date
  - author: get posts with the given author
  - blog_id: get posts for the blog with the given id
  - tags: get posts with the given tags - impletment later
