from django.db import models
from django.contrib.auth.models import User

class Tag(models.Model):
  text = models.CharField(max_length=100, primary_key=True)

class Picture(models.Model):
  name = models.CharField(max_length=50)
  description = models.CharField(max_length=200)
  url = models.SlugField()

class Blog(models.Model):
  owner = models.ForeignKey(User)
  name = models.CharField(max_length=50)
  url = models.SlugField()
  date_created = models.DateTimeField(auto_now_add=True)
  allows_comments = models.BooleanField(default=True)

  def __unicode__(self):
    return "" + self.name

class Post(models.Model):
  author = models.ForeignKey(User)
  blog = models.ForeignKey(Blog)
  title = models.CharField(max_length=50)
  url = models.SlugField()
  date_created = models.DateTimeField(auto_now_add=True)
  date_modified = models.DateTimeField(auto_now=True)
  date_published = models.DateTimeField(null=True)
  is_published = models.BooleanField(default=False)
  allows_comments = models.BooleanField(default=True)
  tags = models.ManyToManyField(Tag)
  content = models.TextField()
  pictures = models.ManyToManyField(Picture)

  def __unicode__(self):
    return self.title + " - " + self.blog.name










