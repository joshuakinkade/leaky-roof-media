$(document).ready(function(){
  $("#userNameField").keyup(handleUserNameKeyup);
  $("#emailField").keyup(handleEmailKeyUp);
  $('#passwordField').keyup(passwordKeyup);
  $('#confirmPasswordField').keyup(confirmPasswordKeyup)
});

function handleEmailKeyUp(event) {
  if (!Validator.isValidEmail(event.target.value)) {
    $(event.target).siblings(".error").addClass("visible");
  }
  else
  {
    $(event.target).siblings(".error").removeClass("visible");
  }
}

function handleUserNameKeyup(event) {
  if (!Validator.isValidUsername(event.target.value)) {
    $(event.target).siblings(".error").addClass('visible');
  }
  else
  {
    $(event.target).siblings(".error").removeClass('visible');
  }
}

function passwordKeyup(event) {
  if( !Validator.isValidPassword(event.target.value)) {
    $(event.target).siblings(".error").addClass('visible');
  }
  else
  {
    $(event.target).siblings('.error').removeClass('visible');
  }
}

function confirmPasswordKeyup(event) {

  console.log(event.target.value,$("#passwordField").val());

  if ( !Validator.passwordsMatch(event.target.value,$('#passwordField').val())) {
    $(event.target).siblings('.error').addClass('visible');
  }
  else {
    console.log("Hello");
    $(event.target).siblings('.error').removeClass('visible');
  }
}
