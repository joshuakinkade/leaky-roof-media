from django.conf.urls import patterns, url

urlpatterns = patterns('accounts.views',
    url(r'^signup/$', 'signup'),
    url(r'^logout/$', 'logout'),
    url(r'^login/$', 'login'),
    url(r'^settings/$', 'settings'),
  )
