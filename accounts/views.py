from django.shortcuts import render_to_response, redirect
from django.core.context_processors import csrf
from django.contrib.auth.models import User
from django.contrib.auth import logout as alogout, authenticate, login as alogin

################################################################################
# View - signup
################################################################################
def signup(request):
  
  c = {}
  error = False

  try:
    username = request.POST['userNameField']
    email = request.POST['emailField']
    password = request.POST['passwordField']
    confirm_password = request.POST['confirmPasswordField']
  except Exception, e:
    c.update(csrf(request))
    return render_to_response("accounts/signup.html",c)    

  if len(User.objects.filter(username=username)) > 0:
    print "Username taken"
    error = True
    c.update({'username_taken':True})

  if password != confirm_password:
    print password
    print confirm_password
    error = True
    c.update({'password_mismatch':True})

  if error is True:
    c.update(csrf(request))
    return render_to_response("accounts/signup.html",c)

  user = User.objects.create_user(username,email,password)
  user.save()

  #alogin(request,user)

  return redirect('LeakyRoofMedia.views.home')

################################################################################
# View - logout
################################################################################
def logout(request):
  alogout(request);
  return redirect("LeakyRoofMedia.views.home")

################################################################################
# View - login
################################################################################
def login(request):

  try:
    username = request.POST['userNameField']
    password = request.POST['passwordField']
    next = request.POST['nextField']
  except Exception, e:
    c = {}
    try:
      next = request.GET['next']
      c = {'next':next}
      print next
    except Exception, e:
      pass
    c.update(csrf(request))
    return render_to_response("accounts/login.html",c)

  user = authenticate(username=username, password=password)

  if user is None:
    c= {'login_failed': True,'next': request.POST['nextField']}
    c.update(csrf(request))
    return render_to_response("accounts/login.html",c)

  alogin(request, user)

  return redirect(next)

################################################################################
# View - settings
################################################################################
def settings(request):

  c = {}
  c.update(csrf(request))
  return render_to_response('accounts/settings.html',c)
